#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=48
#SBATCH --time=180:00:00
#SBATCH --partition=long  

module load MATLAB/R2022a
matlab -nosplash -nodisplay -r "master;quit;"