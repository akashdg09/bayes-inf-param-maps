clear;
tic;
reset_path();
num_workers = 48;
parpool(num_workers);
toc;

num_suns = logspace(0,-1,10);
seperation = 31;

% parameter sweeps
num_per_param=25;

N0s = logspace(16,19,num_per_param);
taus = logspace(1,4,num_per_param) ;

%Initial ETL/HTL range
% SRV_ETL = logspace(2,4,num_per_param); 
% SRV_HTL = logspace(2,4,num_per_param); 

%1st extention: Lower HTL range
% SRV_ETL = logspace(2,4,num_per_param); 
% SRV_HTL = logspace(0,2,num_per_param); 

% 2nd extention: Lower ETL range
SRV_ETL = logspace(0,2,num_per_param); 
SRV_HTL =  cat(2,logspace(0,2,num_per_param), logspace(2,4,num_per_param)); 

linear_params = zeros((50*num_per_param^3), 4); % change len based on what you are doing

counter = 1;
for i=1:num_per_param
    for j=1:num_per_param
        for k=1:num_per_param
            for l=1:num_per_param*2
                linear_params(counter,1) = N0s(i);
                linear_params(counter,2) = taus(j);
                linear_params(counter,3) = SRV_ETL(k);
                linear_params(counter,4) = SRV_HTL(l);
                counter = counter + 1;
            end
        end
    end
end

% saves on ram sent to other pools? 
clear N0s;
clear taus;
clear SRV_ETL;
clear SRV_HTL;

N0_flat = linear_params(:,1);
tau_flat = linear_params(:,2);
SRV_ETL_flat = linear_params(:,3);
SRV_HTL_flat = linear_params(:,4);

clear linear_params;

% main: 

filename = 'out';
chunks = chunk(1:length(N0_flat), num_workers); % divide indices into equally sized chunks

tic;
spmd
%parfor  i = 1:length(N0_flat)
    worker_idx = labindex;
    % generate a unique filename for each worker
    worker_filename = ['/data/phys-perovskite/newc6149/Intensity_dep_QFLS_maps/',sprintf('%s_worker%d.csv', filename, worker_idx+96)];

    for i = chunks{worker_idx}
        params = make_params(N0_flat(i)*1e6,tau_flat(i)*1e-9, SRV_ETL_flat(i)*1e-2,SRV_HTL_flat(i)*1e-2);
        try  
            sol  =  numericalsolver(params);
            sol  =  compute_QFLs(sol);
            Efn = sol.dstrbns.Efn(:, int16(size(sol.dstrbns.Efn, 2)/2));
            Efp = sol.dstrbns.Efp(:, int16(size(sol.dstrbns.Efn, 2)/2));
            [G0,n0,p0,ni2,gamma,tor,tor3,R,SRH,Rl,dE,Rr,dH, N, NE,b] = struct2array(sol.params, {'G0','n0','p0','ni2','gamma','tor','tor3','R','SRH','Rl', 'dE','Rr','dH', 'N', 'NE','b'});

            x = sol.vectors.x;
            xE = sol.vectors.xE;
            xH = sol.vectors.xE;

            [nE, pH] = struct2array(sol.dstrbns, {'nE', 'pH'});
            [n, p] = struct2array(sol.dstrbns, {'n','p'});
            n = n/n0;
            p = p/p0;

            R_SRH = G0*SRH(n,p,gamma,ni2,tor,tor3);
            R_L = G0*Rl(nE(:,NE+1)/dE,p(:,1));
            R_R = G0*Rr(n(:,N+1),pH(:,1)/dH);

            time_indexs = zeros(1,length(num_suns));
            [psiE, psi, psiH] = struct2array(sol.dstrbns,{'phiE','phi', 'phiH'});

            for j=1:length(num_suns)
                [m, time_index] = min(abs(sol.time - seperation*(j)));
                time_indexs(1,j) = time_index;
            end

            QFLSs = Efn(time_indexs)-Efp(time_indexs);
            data = cell(length(num_suns)+9, 3);

            k = 1;

            data{k,1} = 'params';
            k = k+1;
            data{k,1} = 'N0';
            data{k,2} = N0_flat(i);
            k = k+1;
            data{k,1} = 'tau';
            data{k,2} = tau_flat(i);
            k = k+1;
            data{k,1} = 'SRV_ETL';
            data{k,2} =  SRV_ETL_flat(i);
            k = k+1;
            data{k,1} = 'SRV_HTL';
            data{k,2} = SRV_HTL_flat(i);
            k = k+1;
            data{k,1} = 'intensity_dep';
            k = k+1;
            for l = k:k+length(time_indexs)-1
                data{l, 1} = num_suns(l-k+1);
                data{l, 2} = QFLSs(l-k+1);

            end

            writecell(data,worker_filename,'WriteMode','append');

        catch  e
            fprintf('There  was  an  error!  The  message  was:\n%s',e.message);
            fprintf('\n');
        end
    end  
end
toc;

%%%%%%%%%%%%%%%% PARAM FUNC

function params = make_params(N0_in, tau_in, SRV_ETL_in, SRV_HTL_in)
    workfolder  = '/data/phys-perovskite/newc6149/Intensity_dep_QFLS_maps/'; % the folder to which data will be saved,
    OutputFcn   = 'PrintTime'; % ode15s optional message function, choose
    Verbose     = false; % set this to false to suppress message output,
    UseSplits   = true; % set this to false to make a single call to ode15s

    % Resolution and error tolerances
    N    = 400; % Number of subintervals, with N+1 being the number of grid points
    rtol = 1e-6; % Relative temporal tolerance for ode15s solver
    atol = 1e-10; % Absolute temporal tolerance for ode15s solver
    phidisp = 100; % displacement factor for electric potential (VT) (optional)

    %% Parameter input

    % Physical constants
    eps0  = 8.854187817e-12;  % permittivity of free space (Fm-1)
    q     = 1.6021766209e-19; % charge on a proton (C)
    Fph   = 1.4e21;           % incident photon flux (m-2s-1)
    kB    = 8.61733035e-5;    % Boltzmann constant (eVK-1)
    kb_real = 1.380649e-23;

    % Perovskite parameters
    T     = 298;       % temperature (K)
    b     = 650e-9;    % perovskite layer width (m) (normally between 150-600nm)
    epsp  = 33*eps0; % permittivity of perovskite (Fm-1)
    alpha = 4970219.22 * 2;     % perovskite absorption coefficient (m-1)
    Ec    = -3.6;      % conduction band minimum (eV)
    Ev    = -5.2;      % valence band maximum (eV)
    Dn    = 1.63e-4;    % perovskite electron diffusion coefficient (m2s-1)
    Dp    = 1.44e-4;    % perovskite hole diffusion coefficient (m2s-1)
    gc    = 1.68e24;    % conduction band density of states (m-3)
    gv    = 1.91e24;    % valence band density of states (m-3)

    % Ion parameters
    N0    = N0_in;        % typical density of ion vacancies (m-3)
    D     = @(Dinf, EA) Dinf*exp(-EA/(kB*T)); % diffusivity relation
    DIinf = 6.5e-8;        % high-temp. vacancy diffusion coefficient (m2s-1)
    EAI   = 0.58;          % iodide vacancy activation energy (eV)
    DI    = D(DIinf, EAI); % diffusion coefficient for iodide ions (m2s-1)

    % Direction of light
    inverted = true; % choose false for a standard architecture cell (light
    % ETL parameters

    gcE   = 1.05e26;    % effective conduction band DoS in ETL (m-3) # from https://onlinelibrary.wiley.com/doi/full/10.1002/adfm.202007738
    EcE   = -3.7;    % conduction band minimum in ETL (eV) # from https://pubs.acs.org/doi/10.1021/acs.nanolett.9b00936
    EfE = EcE - 0.1;
    bE    = 100e-9;  % width of ETL (m)
    epsE  = 3*eps0; % permittivity of ETL (Fm-1) # from https://pubs.acs.org/doi/10.1021/acs.nanolett.9b00936
    muE   = 1e-7;

    % HTL parameters 
    gvH   = 5e24;    % effective valence band DoS in HTL (m-3)
    EvH   = -5.2;    % valence band maximum in HTL (eV)
    EfH = EvH+0.1;
    bH    = 100e-9;  % width of HTL (m)
    epsH  = 3*eps0;  % permittivity of HTL (Fm-1)
    muH   = 7.5e-7;

    % Metal contact parameters (optional)
    % Ect   = -4.1;    % cathode workfunction (eV)
    % Ean   = -5.0;    % anode workfunction (eV)

    % Bulk recombination
    tn    = tau_in;    % electron pseudo-lifetime for SRH (s)
    tp    = tau_in;    % hole pseudo-lifetime for SRH (s)
    beta  = 0;       % bimolecular recombination rate (m3s-1)
    Augn  = 0;       % electron-dominated Auger recombination rate (m6s-1)
    Augp  = 0;       % hole-dominated Auger recombination rate (m6s-1)

    % Interface recombination (max. velocity ~ 1e5)
    betaE = 0;       % ETL/perovskite bimolecular recombination rate (m3s-1)
    betaH = 0;       % perovskite/HTL bimolecular recombination rate (m3s-1)
    vnE   = 1e5;     % electron recombination velocity for SRH (ms-1)
    vpE   = SRV_ETL_in;      % hole recombination velocity for SRH (ms-1)
    vnH   = SRV_HTL_in;     % electron recombination velocity for SRH (ms-1)
    vpH   = 1e5;     % hole recombination velocity for SRH (ms-1)

    % Parasitic resistances (optional)
    Rs    = 3;       % external series resistance (Ohm)
    Rp    = 10000;     % parallel or shunt resistance (Ohm) (can choose Inf)
    % Acell = 1;       % cell area (cm2) (only used to scale the series resistance)

    % Compile all parameters into a convenient structure
    vars = setdiff(who,{'params','vars'});
    for i=1:length(vars), params.(vars{i}) = eval(vars{i}); end

    % Non-dimensionalise the user-defined input parameters
    params = nondimensionalise(params);

    % Unpack variables and functions needed in the rest of this function
    [tstar2t, psi2Vap, Upsilon, Vbi] = struct2array(params, ...
        {'tstar2t','psi2Vap','Upsilon','Vbi'});

    %% Simulation protocol
    light_intensity = {1};
    list_of_suns = logspace(0,-1,10);
    
        for i = list_of_suns
            light_intensity{end+1}='tanh';
            light_intensity{end+1}=1;
            light_intensity{end+1}=i;

            light_intensity{end+1}='linear';
            light_intensity{end+1}=30;
            light_intensity{end+1}=i;
        end

    applied_voltage = {'open-circuit'};

    reduced_output = true; % set to true to reduce the amount of data retained ...
    time_spacing = 'lin'; % set equal to either 'lin' (default) or 'log'

    %% Create the simulation protocol and plot (if Verbose)

    % Create the protocol and time points automatically, psi=(Vbi-Vap)/(2*kB*T)
    [light, psi, time, splits, findVoc] = ...
        construct_protocol(params,light_intensity,applied_voltage,time_spacing);

    % *** If defining one's own simulation protocol, define it here! ***

    % Apply the options defined above
    if inverted, inv = -1; else, inv = 1; end
    if ~UseSplits, splits = time([1,end]); end

    % Define the charge carrier generation function G(x,t)
    Gspec = generation_profile(alpha,Upsilon,inv);
    G = @(x,t) light(t).*ppval(Gspec,x);   

    %% Compile more parameters into the params structure
    vars = setdiff(setdiff(who,fieldnames(params)),{'params','vars','i'});
    for i=1:length(vars), params.(vars{i}) = eval(vars{i}); end

    % Make the folder in which to save the output data (specified above)
    if exist(workfolder,'dir')~=7, mkdir(workfolder); end

    end

    function chunks = chunk(indices, num_chunks)
        % CHUNK Divide indices into equally sized chunks
        %   CHUNK(INDICES, NUM_CHUNKS) divides the indices in the array INDICES into
        %   NUM_CHUNKS equally sized chunks and returns them as a cell array of index
        %   ranges.
        %
        %   Example:
        %       indices = 1:100;
        %       chunks = chunk(indices, 4);
        %       for i = 1:numel(chunks)
        %           disp(indices(chunks{i}));
        %       end
        
            chunk_size = ceil(numel(indices) / num_chunks);
            chunks = cell(1, num_chunks);
            for i = 1:num_chunks
                start_idx = (i-1)*chunk_size + 1;
                end_idx = min(i*chunk_size, numel(indices));
                chunks{i} = start_idx:end_idx;
            end
        end