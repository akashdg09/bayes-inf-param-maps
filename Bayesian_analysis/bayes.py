import numpy as np
import os
import pandas as pd
from joblib import Parallel, delayed, dump, load
import time

tictic = time.time()

# Load preprocessed data
real_data_df = pd.read_pickle('real_data_df')
model_arr = np.load('model_arr.npy')

# Memmap model array
dump(model_arr, './memmap_dump/model_arr_memmap')
model_arr = load('./memmap_dump/model_arr_memmap', mmap_mode='r')

savepath = "./out/posterior"
err = 0.01 # Based on error of measurement

def likleyhood(data_arr, model_arr, posterior, i,j):
    ''' Likleyhood based on bayesian method, using log likleyhood to minimise truncation error'''
    data_arr = np.broadcast_to(data_arr[i,j,:], model_arr.shape)
    log_likleyhood = (model_arr - data_arr)**2/(2*err**2) 
    likleyhood = np.exp(-1*np.sum(log_likleyhood, axis=-1))
    # Super rough normalisation, just to avoid truncation
    posterior[i,j] = likleyhood / np.sum(likleyhood)

saveshape = {'Sample':[], 'Aging_time': [], 'shape':[]}

for _,row in real_data_df.iterrows():
    if not os.path.isdir(f"{savepath}/{int(row['Aging_time'])}"):
        os.makedirs(f"{savepath}/{int(row['Aging_time'])}")
    
    full_data = row['Data']
    for ind, dataslice in zip(row['Slice_label'], row['Slices']):
        print('Processing:', f"{ind}_{'_'.join(row['Sample'].split(' '))}", f"{int(row['Aging_time'])}h")
        tic = time.time()
        meas = full_data[dataslice[0]:dataslice[1], dataslice[2]:dataslice[3],:]
        dump(meas, './memmap_dump/meas_memmap')
        meas = load('./memmap_dump/meas_memmap', mmap_mode='r')
        
        posterior = np.memmap(f"{savepath}/{int(row['Aging_time'])}/{ind}_{'_'.join(row['Sample'].split(' '))}", 
                              shape=(meas.shape[0], meas.shape[1], model_arr.shape[0], model_arr.shape[1], model_arr.shape[2], model_arr.shape[3]), 
                              dtype=np.float64,
                              mode='w+')

        Parallel(n_jobs=64, backend='threading')(delayed(likleyhood)(meas,model_arr,posterior,i,j) for i in range(meas.shape[0]) for j in range(meas.shape[1]))
        saveshape['Sample'].append(f"{ind}_{'_'.join(row['Sample'].split(' '))}")
        saveshape['Aging_time'].append(int(row['Aging_time']))
        saveshape['shape'].append((meas.shape[0], meas.shape[1], model_arr.shape[0], model_arr.shape[1], model_arr.shape[2], model_arr.shape[3]))
        
        toc = time.time()
        print('Done in:', toc-tic,'s')

pd.DataFrame(saveshape).to_pickle("posteroior_memmap_shapes")

toctoc = time.time()
print('Total time taken:', toctoc-tictic)     

'''
i = image i
j = image j
k = N0
l = tau
m = SRV_ETL
n = SRV_HTL
o = QFLS
'''