import numpy as np
import os
import pandas as pd
from joblib import Parallel, delayed
import time


real_data_df = pd.read_pickle('real_data_df')
model_df = pd.read_pickle('model_df')
memmmap_shape_df = pd.read_pickle('posteroior_memmap_shapes')
model_arr = np.load('model_arr.npy')

N0_ax = np.array(sorted(list(set(model_df['N0'].values))))
tau_ax = np.array(sorted(list(set(model_df['tau'].values))))
SRV_ETL_ax = np.array(sorted(list(set(model_df['SRV_ETL'].values))))
SRV_HTL_ax = np.array(sorted(list(set(model_df['SRV_HTL'].values))))

posteriorpath = "./out/posterior"
savepath_dist = "./out/infered_dist"
savepath_expected = "./out/infered_expected"


def calculate_inf_values(i, j, posterior,
                         N0_inf_dist,tau_inf_dist,SRV_ETL_inf_dist,SRV_HTL_inf_dist,
                         N0_inf_expected,tau_inf_expected,SRV_ETL_inf_expected,SRV_HTL_inf_expected):

    # Colapse posterior to 1 dimetion
    N0_inf_dist[i,j,:] = np.sum(posterior[i, j, :, :, :, :], axis=(1, 2, 3))
    tau_inf_dist[i,j,:] = np.sum(posterior[i, j, :, :, :, :], axis=(0, 2, 3))
    SRV_ETL_inf_dist[i,j,:] = np.sum(posterior[i, j, :, :, :, :], axis=(0, 1, 3))
    SRV_HTL_inf_dist[i,j,:] = np.sum(posterior[i, j, :, :, :, :], axis=(0, 1, 2))
    
    # Renormalise to density
    N0_inf_dist[i,j,:]  /= np.trapz(N0_inf_dist[i,j,:], np.log10(N0_ax))
    tau_inf_dist[i,j,:]  /= np.trapz(tau_inf_dist[i,j,:], np.log10(tau_ax))
    SRV_ETL_inf_dist[i,j,:]  /= np.trapz(SRV_ETL_inf_dist[i,j,:], np.log10(SRV_ETL_ax))
    SRV_HTL_inf_dist[i,j,:]  /= np.trapz(SRV_HTL_inf_dist[i,j,:], np.log10(SRV_HTL_ax))
    
    # Calculate expected value
    N0_inf_expected[i,j]= 10** np.trapz(N0_inf_dist[i,j,:]*np.log10(N0_ax),np.log10(N0_ax))
    tau_inf_expected[i,j] = 10** np.trapz(tau_inf_dist[i,j,:]*np.log10(tau_ax),np.log10(tau_ax))
    SRV_ETL_inf_expected[i,j] = 10** np.trapz(SRV_ETL_inf_dist[i,j,:]*np.log10(SRV_ETL_ax),np.log10(SRV_ETL_ax))
    SRV_HTL_inf_expected[i,j] = 10** np.trapz(SRV_HTL_inf_dist[i,j,:]*np.log10(SRV_HTL_ax),np.log10(SRV_HTL_ax))

tictic = time.time()

for _,row in real_data_df.iterrows():
    for ind, dataslice in zip(row['Slice_label'], row['Slices']):
        
        tic =time.time()
        print('Processing: ', f"{ind}_{'_'.join(row['Sample'].split(' '))}")
        for  i in ['N0', 'tau', 'SRV_ETL', 'SRV_HTL']:
            if not os.path.isdir(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/{i}"):
                os.makedirs(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/{i}")
            if not os.path.isdir(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/{i}"):
                os.makedirs(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/{i}")
        
        samplename=f"{ind}_{'_'.join(row['Sample'].split(' '))}"
        
        mmap_shape = memmmap_shape_df.query(f"Sample=='{samplename}' &  Aging_time=={int(row['Aging_time'])}")['shape'].values[0]
        posterior_path =f"{posteriorpath}/{int(row['Aging_time'])}/{ind}_{'_'.join(row['Sample'].split(' '))}"
        posterior = np.memmap(posterior_path, mode='r', shape = mmap_shape, dtype=np.float64)

        N0_inf_dist = np.memmap('./memmap_dump/N0_inf_dist_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1], N0_ax.size), mode='w+')
        tau_inf_dist = np.memmap('./memmap_dump/tau_inf_dist_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1], tau_ax.size), mode='w+')
        SRV_ETL_inf_dist = np.memmap('./memmap_dump/SRV_ETL_dist_inf_memmap', np.float64, shape=(mmap_shape[0], mmap_shape[1], SRV_ETL_ax.size), mode='w+')
        SRV_HTL_inf_dist = np.memmap('./memmap_dump/SRV_HTL_dist_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1], SRV_HTL_ax.size), mode='w+')
        
        N0_inf_expected = np.memmap('./memmap_dump/N0_inf_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1]), mode='w+')
        tau_inf_expected = np.memmap('./memmap_dump/tau_inf_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1]), mode='w+')
        SRV_ETL_inf_expected = np.memmap('./memmap_dump/SRV_ETL_inf_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1]), mode='w+')
        SRV_HTL_inf_expected = np.memmap('./memmap_dump/SRV_HTL_memmap', dtype=np.float64, shape=(mmap_shape[0], mmap_shape[1]), mode='w+')


        Parallel(n_jobs=64)(delayed(calculate_inf_values)(i, j, posterior,
                                                          N0_inf_dist,tau_inf_dist,SRV_ETL_inf_dist,SRV_HTL_inf_dist,
                                                          N0_inf_expected,tau_inf_expected,SRV_ETL_inf_expected,SRV_HTL_inf_expected) for i in range(mmap_shape[0]) for j in range(mmap_shape[1]))
        
        
        np.save(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/N0/N0_{int(row['Aging_time'])}", N0_inf_dist)
        np.save(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/tau/tau_{int(row['Aging_time'])}", tau_inf_dist)
        np.save(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/SRV_ETL/SRV_ETL_{int(row['Aging_time'])}", SRV_ETL_inf_dist)
        np.save(f"{savepath_dist}/{'_'.join(row['Sample'].split(' '))}/{ind}/SRV_HTL/SRV_HTL_{int(row['Aging_time'])}", SRV_HTL_inf_dist)
        
        np.save(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/N0/N0_{int(row['Aging_time'])}", N0_inf_expected)
        np.save(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/tau/tau_{int(row['Aging_time'])}", tau_inf_expected)
        np.save(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/SRV_ETL/SRV_ETL_{int(row['Aging_time'])}", SRV_ETL_inf_expected)
        np.save(f"{savepath_expected}/{'_'.join(row['Sample'].split(' '))}/{ind}/SRV_HTL/SRV_HTL_{int(row['Aging_time'])}", SRV_HTL_inf_expected)
        toc = time.time()
        print('Done in:', toc-tic,'s')
        
toctoc = time.time()
print('Total time taken:', toctoc-tictic)   

# Cleanup      
for _,_,filenames in os.walk('./memmap_dump'):
    for filename in filenames:
        os.remove(f'./memmap_dump/{filename}')
    break