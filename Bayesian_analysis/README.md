# Bayesian inference

* These scripts combine previously generated drift diffusion simulations of intensity depandant QFLS images

## Data format

* The scripts will crawl folders for intensity depadant QFLS maps
* Fileformat is .npy arrays
* The expected filename format: OC_[number of suns]_[photon flux]_.npy
* Paths are hard coded
* Also loads Qcol data, here the format should be the same, but just 1 file per folder

* The simulations should be in csv files
* The scripts expects a folder with csvs, where the filename is in the format out_worker_[index].csv
* csv format is the same as the save files in the drift diffusion component found in top level directory

## Usage

* These scripts heavily use mmaping so make sure you have a fast ssd!
* Run generate datastore to ingest data and save it in a format the other scriots are expecting
* Run bayes to create (large!) posterior arrays: They have 2 spatial dimentions for the image, and 4 parameter dims
* Run infered_maps finally to colapse the posterior array to something useful: 4 arrays with 1 parameter dim, and 4 maps of expected value 


