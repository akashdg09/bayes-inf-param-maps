import numpy as np
import os
from scipy.interpolate import interp1d
import csv
import pandas as pd
from joblib import Parallel, delayed
import time

tic = time.time()

def data_loader(datapath):
    '''
    Loads the intensity depandant QFLS maps, concatenates to one data array
    '''
    print('loading:', datapath)
    # Crawls folder and finds files
    for _,_,filenames in os.walk(datapath):
        break
    arr_shape = np.load(f"{datapath}/{filenames[0]}").shape
    
    data_arr = np.zeros(list(arr_shape)+[len(filenames)])
    num_suns_data = []
    k_used = []

    for k, filename in enumerate(filenames):
        if float(filename.split('_')[1]) > 1.1  or float(filename.split('_')[1]) <0.08:
            continue
        arr = np.load(f"{datapath}/{filename}") 
        data_arr[:,:,k] = arr
        num_suns_data.append(float(filename.split('_')[1]))
        k_used.append(k)

    data_arr = data_arr[:,:,np.array(k_used)]

    # Sort by numsuns
    ind = [i for i in range(len(num_suns_data))]
    ind = [i for _,i in sorted(zip(num_suns_data, ind), reverse=True)]
    data_arr_sorted = data_arr[:,:,ind]

    return data_arr_sorted, np.array(sorted(num_suns_data, reverse=True))

######## Load Yen's data

samples = ["ref-good-97/4","ref-bad-98/4","mod-D0/6"]
sample_labels = ['Ref 1', 'Ref 2', 'Mod']
ages = ['0000_00h','0096_00h','0300_00h','0600_00h','1200_00h']

num_suns = np.logspace(0,-1,10)

names = []
ages2 = []
datas = []
data_Qcols = []

for sample_label, sample in zip(sample_labels, samples):
    for age in ages:
        if not os.path.isdir(f"/home/akashdasgupta/Documents/Yen_data/{age}/{sample}/QFLS_oc_int_dep"):
            continue
        data, data_nsun = data_loader(f"/home/akashdasgupta/Documents/Yen_data/{age}/{sample}/QFLS_oc_int_dep") 
        data_inter = interp1d(data_nsun,data, axis=2)(num_suns)

        names.append(sample_label)
        ages2.append(float('.'.join(age.split('h')[0].split('_'))))
        datas.append(data_inter)

        for _,_,filenames in os.walk(f"/home/akashdasgupta/Documents/Yen_data/{age}/{sample}/Q_col"):
            data_Qcols.append(np.load(f"/home/akashdasgupta/Documents/Yen_data/{age}/{sample}/Q_col/{filenames[0]}"))

            break
real_data_df = pd.DataFrame({'Sample': names, 'Aging_time': ages2, 'Data': datas, 'Qcol_data': data_Qcols})

######### Data slices

dataslices = []
slice_labels = []
for _,row in real_data_df.iterrows():
    samplename = row['Sample']
    if samplename == 'Mod':
        dataslice = [(350,400,200,250)] #rmin, rmax
        slice_label = [1]
    elif samplename == 'Ref 1':
        dataslice = [(150,200,150,200),
                      (290,340,10,60)] #rmin, rmax
        slice_label = [2,3]
    elif samplename == 'Ref 2':
        dataslice = [(150,200,210,260),
                      (375,425,110,160)] #rmin, rmax
        slice_label = [4,5]

    dataslices.append(dataslice)
    slice_labels.append(slice_label)
    
real_data_df['Slices'] = dataslices
real_data_df['Slice_label'] = slice_labels

real_data_df.to_pickle("real_data_df")
toc = time.time()
print('Done in: ', toc-tic,'s')
tic = toc

### Load simulatons 

print('working on model array (may take a few mins...)')
modelpath = "/home/akashdasgupta/Documents/sim_data"

N0_list = []
tau_list = []
SRV_ETL_list = []
SRV_HTL_list = []
QFLSs = []

for _,_,filenames in os.walk(f"{modelpath}"):
    for filename in filenames:
        with open(f"{modelpath}/{filename}", 'r') as file:
            reader = list(csv.reader(file))
            for i,row in enumerate(reader):
                if row[0] == 'params':
                    N0_list.append(float(reader[i+1][1]))
                    tau_list.append(float(reader[i+2][1]))
                    SRV_ETL_list.append(float(reader[i+3][1]))
                    SRV_HTL_list.append(float(reader[i+4][1]))
                    QFLSs.append(np.array([float(reader[i+6+j][1]) for j in range(10)]))
    break
model_df_unfiltered = pd.DataFrame({'N0': N0_list,
                        'tau':tau_list,
                        'SRV_ETL': SRV_ETL_list,
                        'SRV_HTL': SRV_HTL_list,
                        'QFLS':QFLSs})
model_df_unfiltered.drop_duplicates(subset=['N0','tau','SRV_ETL','SRV_HTL'])
# we simulated ETL SRV down to 10^0 but that skews the distribution, so limiting it to 10^1
model_df = model_df_unfiltered[model_df_unfiltered['SRV_ETL'] >= 10] 
model_df.to_pickle("model_df")

N0_ax = sorted(list(set(model_df['N0'].values)))
tau_ax = sorted(list(set(model_df['tau'].values)))
SRV_ETL_ax = sorted(list(set(model_df['SRV_ETL'].values)))
SRV_HTL_ax = sorted(list(set(model_df['SRV_HTL'].values)))

model_arr = np.zeros([len(N0_ax),len(tau_ax),len(SRV_ETL_ax),len(SRV_HTL_ax), len(num_suns)])

for _,row in model_df.iterrows():
    i = np.argmin([abs(o-row['N0']) for o in N0_ax])
    j = np.argmin([abs(o-row['tau']) for o in tau_ax])
    k = np.argmin([abs(o-row['SRV_ETL']) for o in SRV_ETL_ax])
    l = np.argmin([abs(o-row['SRV_HTL']) for o in SRV_HTL_ax])
    
    model_arr[i,j,k,l,:] = row['QFLS']

np.save('model_arr', model_arr)
toc = time.time()
print('Done in: ', toc-tic,'s')